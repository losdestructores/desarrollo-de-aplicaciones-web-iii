<?php
function esPalindromo($palabra)
{
    $palabraInvertida = strrev($palabra);
    if ($palabra == $palabraInvertida) {
        return true;
    } else {
        return false;
    }
}

if (isset($_POST['enviar'])) {
    $palabra = $_POST['palabra'];
    if (esPalindromo($palabra)) {
        echo "<p>$palabra es palíndromo</p>";
    } else {
        echo "<p>$palabra no es palíndromo</p>";
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Determinar si una palabra es palíndromo</title>
    <meta charset="UTF-8">
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            flex-direction: column;
            background-image: radial-gradient(circle at 68.46% 50%, #ff18c6 0, #ff11cf 10%, #ff13d5 20%, #ff19d7 30%, #fc1fd5 40%, #d723ce 50%, #b126c5 60%, #8e28bc 70%, #6b2ab3 80%, #472cac 90%, #0d2da5 100%);
        }

        input[type=text] {
            padding: 10px;
            font-size: 16px;
        }

        button {
            padding: 10px 20px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 16px;
        }
    </style>
</head>

<body>
    <form method="post">
        <label for="palabra">Introduce una palabra:</label>
        <input type="text" name="palabra" required>
        <button type="submit" name="enviar">Comprobar</button>
    </form>
</body>

</html>