<!DOCTYPE html>
<html>

<head>
    <title>Suma de dígitos pares e impares</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="estilo3.css">
</head>

<body>
    <form method="post">
        <label for="numero">Número:</label>
        <input type="number" name="numero" required>
        <button type="submit">Calcular suma de dígitos pares e impares</button>
    </form>
    <?php
    function suma_digitos($numero)
    {
        $suma_pares = 0;
        $suma_impares = 0;
        while ($numero != 0) {
            $digito = $numero % 10;
            if ($digito % 2 == 0) {
                $suma_pares += $digito;
            } else {
                $suma_impares += $digito;
            }
            $numero = intval($numero / 10);
        }
        echo "<div class='respuesta'>La suma de los dígitos pares es: $suma_pares</div>";
        echo "<div class='respuesta'>La suma de los dígitos impares es: $suma_impares</div>";
    }
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $numero = intval($_POST['numero']);
        suma_digitos($numero);
    }
    ?>
</body>

</html>