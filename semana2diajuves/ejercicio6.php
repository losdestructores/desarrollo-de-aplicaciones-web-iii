<!DOCTYPE html>
<html>

<head>
    <title>Verificar tipo de carácter</title>
    <meta charset="UTF-8">
</head>

<body>
    <link rel="stylesheet" href="estilo6.css">
    <form method="post">
        <label for="caracter">Introduce un carácter:</label>
        <input type="text" name="caracter" required>
        <br>
        <button type="submit">Verificar</button>
    </form>
    <?php
    function verificar_caracter($caracter)
    {
        if (ctype_alpha($caracter)) {
            if (ctype_upper($caracter)) {
                echo "<p>El carácter es una letra mayúscula.</p>";
            } else {
                echo "<p>El carácter es una letra minúscula.</p>";
            }
        } elseif (ctype_digit($caracter)) {
            echo "<p>El carácter es un número.</p>";
        } elseif (ctype_punct($caracter)) {
            echo "<p>El carácter es un símbolo.</p>";
        } else {
            echo "<p>No se pudo determinar el tipo de carácter.</p>";
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $caracter = $_POST['caracter'];
        verificar_caracter($caracter);
    }
    ?>
</body>

</html>