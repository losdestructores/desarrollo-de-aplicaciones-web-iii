
<html>

<head>
    <title>Área y perímetro de un cuadrado</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
    <link rel="stylesheet" href="estilo1.css">

    <?php
    

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $lado = floatval($_POST['lado']);
        $area = $lado * $lado;
        $perimetro = $lado * 4;
        echo 'Valor del area: ' . $area . "<br/>\n";
        echo 'Valor del perimetro: ' . $perimetro . "<br/>\n";
    }

    ?>
    <form method="post">
        <table style="text-align: left; margin-left: auto; margin-right: auto;" border="1" cellpadding="1" cellspacing="1">
            <tbody>
                <tr>
                    <td>
                        <label for="lado">Ingresa el valor de lado:</label>
                    </td>
                    <td>
                        <input name="lado" required="required" step="0.000001" type="number" />
                    </td>
                </tr>
                <tr align="center">
                    <td colspan="2" rowspan="1">
                        <input value="Procesar" type="submit" />
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</body>

</html>