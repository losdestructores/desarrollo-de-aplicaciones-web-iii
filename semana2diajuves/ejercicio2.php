<!DOCTYPE html>
<html>

<head>
    <title>Etapa de la vida</title>
    <meta charset="UTF-8">
</head>

<body>
    <link rel="stylesheet" href="estio2.css">
    <form method="post">
        <label for="edad">Edad:</label>
        <input type="number" name="edad" required>
        <button type="submit">Determinar etapa</button>
    </form>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $edad = intval($_POST['edad']);
        if ($edad >= 0 && $edad <= 2) {
            echo "La persona se encuentra en la etapa de bebé.";
        } elseif ($edad >= 3 && $edad <= 5) {
            echo "La persona se encuentra en la etapa de niño.";
        } elseif ($edad >= 6 && $edad <= 12) {
            echo "La persona se encuentra en la etapa de pubertad.";
        } elseif ($edad >= 13 && $edad <= 18) {
            echo "La persona se encuentra en la etapa de adolescente.";
        } elseif ($edad >= 19 && $edad <= 25) {
            echo "La persona se encuentra en la etapa de joven.";
        } elseif ($edad >= 26 && $edad <= 60) {
            echo "La persona se encuentra en la etapa de adulto.";
        } else {
            echo "La persona se encuentra en la etapa de anciano.";
        }
    }
    ?>
</body>

</html>