<!DOCTYPE html>
<html>

<head>
    <title>Calculadora de descuentos y recargos</title>
    <meta charset="UTF-8">
</head>

<body>
    <link rel="stylesheet" href="estilo4.css">
    <form method="post">
        <label for="monto">Monto de la compra:</label>
        <input type="number" name="monto" required>
        <br>
        <label for="tipo_cliente">Tipo de cliente:</label>
        <select name="tipo_cliente" required>
            <option value="G">Público en general</option>
            <option value="A">Cliente afiliado</option>
        </select>
        <br>
        <label for="forma_pago">Forma de pago:</label>
        <select name="forma_pago" required>
            <option value="C">Al contado</option>
            <option value="P">En plazos</option>
        </select>
        <br>
        <button type="submit">Calcular</button>
    </form>
    <?php
    function Recargo($tipo_cliente)
    {
        if ($tipo_cliente == 'G') {
            return 10;
        } else {
            return 5;
        }
    }
    function Descuento($tipo_cliente)
    {
        if ($tipo_cliente == 'G') {
            return 15;
        } else {
            return 20;
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $monto = floatval($_POST['monto']);
        $tipo_cliente = $_POST['tipo_cliente'];
        $forma_pago = $_POST['forma_pago'];

        if ($forma_pago == 'C') {
            $descuento_o_recargo = $monto * Descuento($tipo_cliente) / 100;
            $total = $monto - $descuento_o_recargo;
        } else {
            $descuento_o_recargo = $monto * Recargo($tipo_cliente) / 100;
            $total = $monto + $descuento_o_recargo;
        }

        echo "<p>El monto a pagar a plazos es: $descuento_o_recargo</p>";
        echo "<p>El total de pagar es: $total</p>";
    }
    ?>
</body>

</html>